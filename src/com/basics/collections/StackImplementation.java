package com.basics.collections;

import java.util.Iterator;
import java.util.Stack;
import java.util.Scanner;

/*
 * Write a program to perform Stack implementation of type string
 * Use the iterator method to print the stack elements.
 */
public class StackImplementation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Stack<String> stack = new Stack();
		stack.push("Hello");
		stack.push("Hii");
		stack.push("Everyone");
		stack.push("Cheers");
		stack.push("Goodbye");
		
		stack.pop();
		
		Iterator iterator = stack.iterator();
		System.out.println("List is");
		
		while(iterator.hasNext())
		{
			System.out.println(iterator.next()+"");
		}
		
		
	}

}
