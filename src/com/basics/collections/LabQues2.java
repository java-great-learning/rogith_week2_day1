package com.basics.collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

public class LabQues2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Write a program to accept user input of type integer and store it in a linkedList.
		 * Display the entered elements in sorted order {ascending and descending both}
		 * Hint : { user Collections.reverseOrder()) to sort linked list in reverse order.}
		 * Use while loop to check whether the input is an integer or not
		 */
		List list = new LinkedList<>();
		Scanner sc = new Scanner(System.in);
		int num,size;
		System.out.println("Enter Size of the List");
		size = sc.nextInt();
		System.out.println("Enter elements of the List");
		for(int i=0;i<size;i++)
		{
			num = sc.nextInt();
			list.add(num);
		}
		
		Collections.sort(list);
		System.out.println(list);
		
		Collections.reverse(list);
		System.out.println(list);
		
		
	}

}
