package com.basics.collections;
import java.util.Comparator;

import invoiceproject.bean.Customer;

public class ImplementComparator implements Comparator<Customer> {

	@Override
	public int compare(Customer o1, Customer o2) {
		// TODO Auto-generated method stub
		
		return o1.getCustomerName().compareTo(o2.getCustomerName());
	}

}
