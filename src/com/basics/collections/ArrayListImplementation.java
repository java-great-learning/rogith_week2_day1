package com.basics.collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
/**
 * Create a class to perform Array List Implementation.
 *Create ArrayList object of type integer and perform following operations
 *Add elements in the arrayList
 *Print the size of the arrayList
 *Remove the arrayList element using index
 *Remove the arrayList element by passing the value in remove method
 *Check the size of the arrayList
 *Find the min element in the arrayList
 *Find the max element in the arrayList
 *Print all the elements using an enhanced for loop
 * @author Roh
 *
 */

public class ArrayListImplementation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num;
		
		List<Integer> list = new ArrayList();
		list.add(10);
		list.add(20);
		list.add(40);
		list.add(30);
		list.add(100);
		
		System.out.println(list.size());
		
		list.remove(2);
		list.remove(Integer.valueOf(30));
		
		
		System.out.println(list.size());
		Collections.sort(list);
		System.out.println(Collections.min(list));
		System.out.println(Collections.max(list));
		Scanner sc = new Scanner(System.in);
		do
		{
		num = sc.nextInt();
		list.add(num);
		}while(num!=0);
		
		
		for(int no: list)
		{
			System.out.println(no);
		}
	}

}
