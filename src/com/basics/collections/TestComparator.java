package com.basics.collections;
import java.util.*;
import java.util.List;
import java.util.Collections ;

import invoiceproject.bean.Customer;

import java.util.ArrayList;
public class TestComparator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Customer> customer = new ArrayList<Customer>();
		customer.add(new Customer("Rogith","Chennai","R@gmail.com",9080800));
		customer.add(new Customer("Angle","Mumbai","A@gmail.com",9080350));
		customer.add(new Customer("Mystic","Bangalore","M@gmail.com",90832400));
		customer.add(new Customer("Busu busu","Chennai","B@gmail.com",92420800));
		
		Collections.sort(customer, new ImplementComparator());
		
		System.out.println("Sorted Arrays");
		for(int i=0;i<customer.size();i++)
		{
			System.out.println(customer.get(i));
		}
	}

}
