package invoiceproject.bean;

public class Customer {
	public String customerName;
	private String customerAddress;
	private String customerMailId;
	private int phone;
	public Customer(String customerName, String customerAddress, String customerMailId, int phone) {
		super();
		this.customerName = customerName;
		this.customerAddress = customerAddress;
		this.customerMailId = customerMailId;
		this.phone = phone;
	}
	public Customer() {
		// TODO Auto-generated constructor stub
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerMailId() {
		return customerMailId;
	}
	public void setCustomerMailId(String customerMailId) {
		this.customerMailId = customerMailId;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	
	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + ", customerAddress=" + customerAddress + ", customerMailId="
				+ customerMailId + ", phone=" + phone + "]";
	}
	
	
	
	

}
