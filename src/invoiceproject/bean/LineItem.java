package invoiceproject.bean;

public class LineItem {
	private Product product;
	private int qty;
	private double tot;
	
	public LineItem(Product product, int qty) {
		super();
		this.product = product;
		this.qty = qty;
	}
	
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}


	@Override
	public String toString() {
		return "LineItem [product=" + product + ", qty=" + qty + ", tot=" + tot + "]";
	}
	
	
	
	

}
